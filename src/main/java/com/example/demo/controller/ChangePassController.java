package com.example.demo.controller;

import com.example.demo.domain.Account;
import com.example.demo.domain.Customer;
import com.example.demo.repository.AccountRepository;
import com.example.demo.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller

public class ChangePassController {
    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private AccountRepository accountRepository;
    @GetMapping("change_pass")
    public String changePassForm(Model model, HttpSession session) {
        model.addAttribute("customer", customerRepository.findByEmailIdIgnoreCase((String) session.getAttribute("username")));
        return "form_change_pass";
    }

    @PostMapping("change_pass")
    public String Save(Model model, HttpServletRequest request, HttpSession session) {
        String username = (String) session.getAttribute("username");
        String newPass = request.getParameter("password");
        Customer customer = customerRepository.findByEmailIdIgnoreCase(username);
        customer.setPassword(newPass);
        customerRepository.save(customer);
        Account entity = accountRepository.getById(username);
        entity.setPassword(newPass);
        accountRepository.save(entity);
        model.addAttribute("customer", customer);
        model.addAttribute("message", "Doi mat khau thanh cong");
        return "form_change_pass";
    }
}
