package com.example.demo.controller.user;


import com.example.demo.domain.Category;
import com.example.demo.domain.Product;
import com.example.demo.model.PriceRange;
import com.example.demo.model.ProductDto;
import com.example.demo.repository.CategoryRepository;
import com.example.demo.repository.ProductRepository;
import com.example.demo.service.ProductService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
@RequestMapping("")
public class HomeController {
    @Autowired
    private ProductService productService;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private CategoryRepository categoryRepository;
    @RequestMapping("")
    public String list(ModelMap model) {
        Category laptop = categoryRepository.getById(1L);
        Category phone = categoryRepository.getById(2L);
        Category pc = categoryRepository.getById(3L);
        List<Product> laptopBestseller = productRepository.findByCategoryAndStatus(laptop, (short) 2);
        List<Product> phoneBestseller = productRepository.findByCategoryAndStatus(phone, (short) 2);
        model.addAttribute("laptop", laptopBestseller);
        model.addAttribute("phone", phoneBestseller);
        return "user/index";
    }

    @GetMapping("view/{productId}")
    public ModelAndView viewDetail(ModelMap model, @PathVariable("productId") Long productId) {
        Optional<Product> optional = productService.findById(productId);
        ProductDto dto = new ProductDto();
        if (optional.isPresent()) {
            Product entity = optional.get();
            BeanUtils.copyProperties(entity, dto);
            dto.setCategoryId(entity.getCategory().getCategoryId());
            model.addAttribute("product", dto);
            return new ModelAndView("user/product/view", model);
        }
        model.addAttribute("message", "Product is not exitsted");
        return new ModelAndView("forward:/index", model);
    }


    @RequestMapping(value = "all_product", method = {RequestMethod.GET, RequestMethod.POST})
    public String allProduct(Model model, @RequestParam(value = "sortBy") Optional<String> sortByRaw,
                                @RequestParam(value = "name") Optional<String> nameRaw, @RequestParam(value = "category") Optional<String> categoryRaw,
                                @RequestParam(value = "minVal", required = false) String minVal, @RequestParam(value = "maxVal", required = false) String maxVal,
                                @RequestParam("page") Optional<Integer> page) {
        int currentPage = page.orElse(1);
        int pageSize = 6;
        Double minVal_number = 100d, maxVal_number = 9999d;
        if (minVal != null && minVal.length() > 0) {
            minVal_number = Double.parseDouble(minVal);
        }
        if (maxVal != null && maxVal.length() > 0) {
            maxVal_number = Double.parseDouble(maxVal);
        }

        String name = nameRaw.orElse("");
        Pageable pageable = PageRequest.of(currentPage-1, pageSize, Sort.by(sortByRaw.orElse("name")));
        String category = categoryRaw.orElse("");
        Page<Product> resultPage = null;
        if (category.equalsIgnoreCase("") || category.isEmpty() || category == null) {
         resultPage = productRepository.findByNameContainingAndUnitPriceBetweenAndStatusNot(name, minVal_number, maxVal_number, (short) 0, pageable);
        }else {
            Long categoryId = 0L;

            if(category.equalsIgnoreCase("phone")) {
                categoryId = 2L;
            }else if(category.equalsIgnoreCase("pc")) {
                categoryId = 3L;
            }else if(category.equalsIgnoreCase("laptop")) {
                categoryId = 1L;
            }
            if (categoryId > 0) {
                Category categoryFilter = categoryRepository.getById(categoryId);
                resultPage = productRepository.findByNameContainingAndCategoryAndUnitPriceBetweenAndStatusNot(name, categoryFilter, minVal_number, maxVal_number, (short) 0, pageable);
            }else resultPage = productRepository.findByNameContainingAndUnitPriceBetweenAndStatusNot(name, minVal_number, maxVal_number, (short) 0, pageable);

        }
        int totalPages = resultPage.getTotalPages();

        if (totalPages > 0) {
            int st = Math.max(1, currentPage - 2);
            int end = Math.min(currentPage + 2, totalPages);

            if (totalPages > pageSize) {
                if (end == totalPages) st = end - pageSize;
                else if (st == 1) end = st + pageSize;
            }

            List<Integer> pageNumbers = IntStream.rangeClosed(st, end)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }
        if (resultPage.getContent().size() == 0) {
            model.addAttribute("message", "Khong tim thay san pham nao");
        }
        model.addAttribute("products", resultPage.getContent());
        model.addAttribute("productPage", resultPage);
        return "user/filter_product";
    }

    @RequestMapping(value = "product/{categoryId}", method = {RequestMethod.GET, RequestMethod.POST})
    public String viewByCategory(Model model, @RequestParam(value = "sortBy") Optional<String> sortByRaw,
                                @RequestParam(value = "name") Optional<String> nameRaw,
                                @PathVariable("categoryId") Long categoryId,
                                @RequestParam(value = "minVal", required = false) String minVal, @RequestParam(value = "maxVal", required = false) String maxVal,
                                @RequestParam("page") Optional<Integer> page) {
        int currentPage = page.orElse(1);
        int pageSize = 6;
        Double minVal_number = 100d, maxVal_number = 9999d;
        if (minVal != null && minVal.length() > 0) {
            minVal_number = Double.parseDouble(minVal);
        }
        if (maxVal != null && maxVal.length() > 0) {
            maxVal_number = Double.parseDouble(maxVal);
        }

        String name = nameRaw.orElse("");
        Pageable pageable = PageRequest.of(currentPage-1, pageSize, Sort.by(sortByRaw.orElse("name")));
        Category categoryFilter = categoryRepository.getById(categoryId);
        Page resultPage = productRepository.findByNameContainingAndCategoryAndUnitPriceBetweenAndStatusNot(name, categoryFilter, minVal_number, maxVal_number, (short) 0, pageable);

        int totalPages = resultPage.getTotalPages();

        if (totalPages > 0) {
            int st = Math.max(1, currentPage - 2);
            int end = Math.min(currentPage + 2, totalPages);

            if (totalPages > pageSize) {
                if (end == totalPages) st = end - pageSize;
                else if (st == 1) end = st + pageSize;
            }

            List<Integer> pageNumbers = IntStream.rangeClosed(st, end)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }
        if (resultPage.getContent().size() == 0) {
            model.addAttribute("message", "Khong tim thay san pham nao");
        }
        model.addAttribute("categoryId", categoryId);
        model.addAttribute("products", resultPage.getContent());
        model.addAttribute("productPage", resultPage);
        return "user/productByType";
    }

}
