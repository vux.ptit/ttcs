package com.example.demo.controller.user;

import com.example.demo.repository.CustomerRepository;
import com.example.demo.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@RequestMapping("info")
@Controller
public class InfoController {
    @Autowired
    private CustomerRepository customerRepository;
    @GetMapping("view")
    public String viewInfo(Model model, HttpSession session) {
        String username = (String) session.getAttribute("username");
        model.addAttribute("customer", customerRepository.findByEmailIdIgnoreCase(username));
        return "user/info/viewInfo";
    }
}
