package com.example.demo.controller.user;

import com.example.demo.domain.*;
import com.example.demo.model.OrderDetailDto;
import com.example.demo.model.OrderDto;
import com.example.demo.repository.CustomerRepository;
import com.example.demo.repository.OrderDetailRepository;
import com.example.demo.repository.OrderRepository;
import com.example.demo.repository.ProductRepository;
import com.example.demo.service.ShoppingCartService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

@Controller

public class BillController {
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private ShoppingCartService cartService;
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private OrderDetailRepository orderDetailRepository;
    @GetMapping("bill")
    public String bill(Model model, HttpSession session) {
        String username = (String) session.getAttribute("username");
        Customer customer = customerRepository.findByEmailIdIgnoreCase(username);
        String pattern = "dd-MM-yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());
        model.addAttribute("dateOfPurchase", date);
        model.addAttribute("customer", customer);
        Collection<Cart> getAll = cartService.getAll();
        ArrayList<Cart> cartItems = new ArrayList<>();
        for (Cart i : getAll) {
            cartItems.add(i);
        }
        double totalPrice = cartService.getAmount();
        model.addAttribute("cartItems", cartItems);
        model.addAttribute("totalPrice", totalPrice);
        // Vi mac dinh phi ship la 2$ nen don hang (0$ + 2$) se khong duong luu
        if (totalPrice > 2) {
            // Luu order
            OrderDto dto = new OrderDto();
            dto.setOrderDate(new Date());
            dto.setCustomer(customer);
            dto.setAmount(totalPrice);
            Order entity = new Order();
            BeanUtils.copyProperties(dto, entity);
            orderRepository.save(entity);

            // Luu order detail
            for (Cart i : cartItems) {
                OrderDetailDto dto1 = new OrderDetailDto();
                Product product = productRepository.getById(i.getProductId());
                // cap nhat lai so luong
                product.setQuantity(product.getQuantity() - i.getQuantity());
                if (product.getQuantity() < 0) {
                    product.setQuantity(0);
                    product.setStatus((short) 0);
                }
                dto1.setOrder(entity);
                dto1.setProduct(productRepository.getById(i.getProductId()));
                dto1.setQuanity(i.getQuantity());
                OrderDetail entity1 = new OrderDetail();
                BeanUtils.copyProperties(dto1, entity1);
                orderDetailRepository.save(entity1);
            }
        }
        cartService.clear();
        return "user/bill";
    }
}
