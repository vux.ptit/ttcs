package com.example.demo.controller.user;

import com.example.demo.domain.Cart;
import com.example.demo.domain.Category;
import com.example.demo.domain.Product;
import com.example.demo.service.ProductService;
import com.example.demo.service.ShoppingCartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;


@Controller
@RequestMapping("carts")
public class CartController {
    @Autowired
    private ProductService productService;

    @Autowired
    private ShoppingCartService cartService;

    HttpSession session;

    @GetMapping("views")
    public String viewsCarts(Model model) {
        model.addAttribute("cartItems", cartService.getAll());
        model.addAttribute("totalPrice", cartService.getAmount());
        model.addAttribute("numberOfItems", cartService.getCount());
        return "user/cart/list";
    }

    @GetMapping("add/{productId}")
    public String addCart(@PathVariable("productId") Long productId) {
        Product product = productService.findById(productId).get();
        if (product != null) {
            Cart item = new Cart();
            item.setProductId(productId);
            item.setName(product.getName());
            item.setImage(product.getImage());
            item.setPrice(product.getUnitPrice());
            item.setQuantity(1);
            cartService.add(item);
        }
        return "redirect:/carts/views";
    }

    @GetMapping("clear")
    public String clearCart() {
        cartService.clear();
        return "redirect:/carts/views";
    }

    @GetMapping("del/{productId}")
    public String removeCart(@PathVariable("productId") Long productId) {
        cartService.remove(productId);
        return "redirect:/carts/views";
    }

    @PostMapping("update")
    public String update(@RequestParam("productId") Long productId, @RequestParam("quantity") Integer quantity){
        cartService.update(productId, quantity);
        return "redirect:/carts/views";
    }
}
