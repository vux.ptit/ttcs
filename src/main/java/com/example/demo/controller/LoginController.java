package com.example.demo.controller;

import com.example.demo.domain.Account;
import com.example.demo.model.LoginAdminDto;
import com.example.demo.service.AccountService;
import com.example.demo.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
public class LoginController {
    @Autowired
    private AccountService accountService;
    @Autowired
    private HttpSession session;
    @Autowired
    private CustomerService customerService;
    @GetMapping("alogin")
    public String login(ModelMap model) {
        session.removeAttribute("username");
        model.addAttribute("account", new LoginAdminDto());
        return "login";
    }

    @PostMapping("alogin")
    public ModelAndView login(ModelMap model,
                              @Valid @ModelAttribute("account") LoginAdminDto dto,
                              BindingResult result) {
        if (result.hasErrors()) {
            return new ModelAndView("login", model);
        }
        Account account = accountService.login(dto.getUsername(), dto.getPassword());
        if (account == null) {
            model.addAttribute("message", "Invalid username or pass");
            return new ModelAndView("login", model);
        }

        session.setAttribute("username", account.getUsername());
        model.addAttribute("email", account.getUsername());
        if(account.isAdmin() == true)
            return new ModelAndView("forward:/admin/categories");
        return new ModelAndView("forward:");
    }


}
