package com.example.demo.controller.admin;

import com.example.demo.domain.Customer;
import com.example.demo.domain.Order;
import com.example.demo.domain.OrderDetail;
import com.example.demo.repository.OrderDetailRepository;
import com.example.demo.repository.OrderRepository;
import com.example.demo.service.OrderDetailService;
import com.example.demo.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.text.SimpleDateFormat;
import java.util.List;

@Controller
@RequestMapping("admin/orders")
public class OrderController {
    @Autowired
    private OrderService orderService;

    @Autowired
    private OrderDetailRepository orderDetailRepository;
    @RequestMapping("")
    public ModelAndView list(ModelMap model) {
        List<Order> list = orderService.findAll();
        if (list == null) {
            model.addAttribute("message", "Khong co don hang nao");
            return new ModelAndView("forward:admin/orders/list", model);
        }
        model.addAttribute("orders", list);
        return new ModelAndView("admin/orders/list", model);
    }


    @GetMapping("detail/{orderId}")
    public ModelAndView viewDetail(ModelMap model, @PathVariable("orderId") Integer orderId) {
        List<OrderDetail> list = orderDetailRepository.findByOrder(orderService.findById(orderId).get());
        Order order = orderService.getById(orderId);
        Customer customer = order.getCustomer();
        String nameCustomer = customer.getFirstName() + " " + customer.getLastName();
        String pattern = "dd-MM-yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String dateOfPurchase = simpleDateFormat.format(order.getOrderDate());
        model.addAttribute("nameCustomer", nameCustomer);
        model.addAttribute("dateOfPurchase", dateOfPurchase);
        model.addAttribute("total", order.getAmount());
        model.addAttribute("orderDetails", list);
        return new ModelAndView("admin/orders/detail", model);
    }
}
