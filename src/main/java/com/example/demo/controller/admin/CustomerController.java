package com.example.demo.controller.admin;

import com.example.demo.domain.Customer;
import com.example.demo.model.AccountDto;
import com.example.demo.model.CustomerDto;
import com.example.demo.service.CustomerService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
@Controller
@RequestMapping("admin/customers")
public class CustomerController {
    @Autowired
    CustomerService customerService;

    @GetMapping("add")
    public String add(Model model) {
        model.addAttribute("customer", new CustomerDto());
        return "admin/customers/addOrEdit";
    }
    @PostMapping("saveOrUpdate")
    public ModelAndView saveOrUpdate(ModelMap model, @Valid @ModelAttribute("customer") CustomerDto dto, BindingResult result) {
        if (result.hasErrors()) {
            return new ModelAndView("admin/customers");
        }
        Customer entity = new Customer();
        BeanUtils.copyProperties(dto, entity);
        customerService.save(entity);
        model.addAttribute("message", "Customer is saved");
        return new ModelAndView("forward:/admin/customers", model);
    }

    @RequestMapping("")
    public ModelAndView list(ModelMap model) {
        List<Customer> list = customerService.findAll();
        if (list == null) {
            model.addAttribute("message", "Cannt found Customer");
            return new ModelAndView("forward:admin/customers/list", model);
        }
        model.addAttribute("customers", list);
        return new ModelAndView("admin/customers/list", model);
    }

    @GetMapping("edit/{id}")
    public ModelAndView edit(ModelMap model, @PathVariable("id") Long id) {
        Optional<Customer> optional = customerService.findById(id);
        CustomerDto dto = new CustomerDto();
        if (optional.isPresent()) {
            Customer entity = new Customer();
            BeanUtils.copyProperties(entity, dto);

            model.addAttribute("customer", dto);

            return new ModelAndView("admin/customers", model);
        }
        model.addAttribute("message", "Customer is not exitsted");
        return new ModelAndView("forward:/admin/customers", model);
    }

    @GetMapping("delete/{id}")
    public ModelAndView delete(ModelMap model, @PathVariable("id") Long id) {
        customerService.deleteById(id);
        model.addAttribute("message", "customer is deleted.");
        return new ModelAndView("forward:/admin/customers", model);
    }
}
