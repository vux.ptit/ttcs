package com.example.demo.controller.admin;

import com.example.demo.domain.Account;
import com.example.demo.model.AccountDto;
import com.example.demo.service.AccountService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("admin/accounts")
public class AccountController {
	
	@Autowired
	AccountService accountService;

	@GetMapping("add")
	public String add(Model model) {
		model.addAttribute("account", new AccountDto());
		return "admin/accounts/addOrEdit";
	}

	@PostMapping("saveOrUpdate")
	public ModelAndView saveOrUpdate(ModelMap model, @Valid @ModelAttribute("account") AccountDto dto, BindingResult result) {
		if (result.hasErrors()) {
			return new ModelAndView("admin/accounts/addOrEdit");
		}
		Account entity = new Account();
		BeanUtils.copyProperties(dto, entity);
		accountService.save(entity);
		model.addAttribute("message", "Account is saved");
		return new ModelAndView("forward:/admin/accounts", model);
	}

	@RequestMapping("")
	public String list(ModelMap model) {
		List<Account> list = accountService.findAll();
		model.addAttribute("accounts", list);
		return "admin/accounts/list";
	}

	@GetMapping("edit/{username}")
	public ModelAndView edit(ModelMap model, @PathVariable ("username") String username) {
		Optional<Account> optional = accountService.findById(username);
		AccountDto dto = new AccountDto();
		if (optional.isPresent()) {
			Account entity = optional.get();
			BeanUtils.copyProperties(entity, dto);

			dto.setIsEdit(true);
			model.addAttribute("account", dto);

			return new ModelAndView("admin/accounts/addOrEdit", model);
		}
		model.addAttribute("message", "Account is not exitsted");
		return new ModelAndView("forward:/admin/accounts", model);
	}

	@GetMapping("delete/{username}")
	public ModelAndView delete(ModelMap model, @PathVariable("username") String username) {
		accountService.deleteById(username);
		model.addAttribute("message", "account is deleted.");
		return new ModelAndView("forward:/admin/accounts", model);
	}
}
