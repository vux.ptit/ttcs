package com.example.demo.controller;

import com.example.demo.domain.Account;
import com.example.demo.domain.ConfirmationToken;
import com.example.demo.domain.Customer;
import com.example.demo.model.AccountDto;
import com.example.demo.repository.AccountRepository;
import com.example.demo.repository.ConfirmationTokenRepository;
import com.example.demo.repository.CustomerRepository;
import com.example.demo.service.AccountService;
import com.example.demo.service.CustomerService;
import com.example.demo.service.impl.EmailServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
public class ForgotController {

    @Autowired
    private CustomerRepository userRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private ConfirmationTokenRepository confirmationTokenRepository;

    @Autowired
    private EmailServiceImpl emailService;

    @Autowired
    AccountService accountService;

    @Autowired
    CustomerService customerService;



    @RequestMapping(value="/forgot_password", method = RequestMethod.GET)
    public ModelAndView displayRegistration(ModelAndView modelAndView, HttpServletRequest request)
    {
        String email = request.getParameter("email");
        modelAndView.addObject("email", email);
        modelAndView.setViewName("forgot_password");
        return modelAndView;
    }



    @RequestMapping(value="/forgot_password", method = RequestMethod.POST)
    public ModelAndView registerUser(ModelAndView modelAndView, String email)
    {

        Customer existingUser = userRepository.findByEmailIdIgnoreCase(email);
        if(existingUser == null)
        {
            modelAndView.addObject("message","Email chua dang ky");
            modelAndView.setViewName("errorEmail");
        }
        else
        {
            String new_pass = Math.round(1000000 * Math.random()) + "";
            ConfirmationToken confirmationToken = new ConfirmationToken(existingUser);
            confirmationTokenRepository.save(confirmationToken);
            SimpleMailMessage mailMessage = new SimpleMailMessage();
            mailMessage.setTo(email);
            mailMessage.setSubject("Forgot Password");
            mailMessage.setFrom("ttcssendmail@gmail.com");
            mailMessage.setText("De xac nhan mat khau moi la:" + new_pass + " vui long, nhan vo link "
                    +"http://localhost:8080/confirm-forgot?token="+confirmationToken.getConfirmationToken()+"&pass="+new_pass);

            emailService.sendEmail(mailMessage);

            modelAndView.addObject("emailId", email);

            modelAndView.setViewName("successfulRegisteration");
        }

        return modelAndView;
    }


    @RequestMapping(value="/confirm-forgot", method= {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView confirmUserAccount(ModelAndView modelAndView, @RequestParam("token")String confirmationToken,
                                            HttpServletRequest request)
    {
        ConfirmationToken token = confirmationTokenRepository.findByConfirmationToken(confirmationToken);

        if(token != null)
        {
            String pass = request.getParameter("pass");
            Customer user = userRepository.findByEmailIdIgnoreCase(token.getUserEntity().getEmailId());
            user.setPassword(pass);
            userRepository.save(user);
            Account entity = accountRepository.getById(token.getUserEntity().getEmailId());
            entity.setPassword(pass);
            accountService.save(entity);
            modelAndView.setViewName("accountVerified");
        }
        else
        {
            modelAndView.addObject("message","The link is invalid or broken!");
            modelAndView.setViewName("errorEmail");
        }

        return modelAndView;
    }




}
