package com.example.demo.service;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Path;

public interface StorageService {

    String getStoredFileName(MultipartFile file, String id);

    void store(MultipartFile file, String storedFileName);

    Resource loadAsRecource(String filename);

    Path load(String filename);

    void delete(String storeFilename) throws IOException;

    void init();
}
