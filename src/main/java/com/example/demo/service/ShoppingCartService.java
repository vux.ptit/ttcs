package com.example.demo.service;

import com.example.demo.domain.Cart;

import java.util.Collection;

public interface ShoppingCartService {
    void add(Cart item);

    Cart update(Long productId, int quantity);

    void remove(Long productId);

    void clear();

    Collection<Cart> getAll();

    double getAmount();

    int getCount();
}
