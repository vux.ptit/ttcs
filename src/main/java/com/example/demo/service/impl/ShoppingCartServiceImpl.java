package com.example.demo.service.impl;

import com.example.demo.domain.Cart;
import com.example.demo.service.ShoppingCartService;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.SessionScope;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@SessionScope
@Service
public class ShoppingCartServiceImpl implements ShoppingCartService {
    Map<Long, Cart> maps = new HashMap<>();

    @Override
    public void add(Cart item) {
        Cart cart = maps.get(item.getProductId());
        if(cart == null) {
            maps.put(item.getProductId(), item);
        }else {
            cart.setQuantity(cart.getQuantity() + 1);
        }
    }

    @Override
    public Cart update(Long productId, int quantity) {
        Cart cart = maps.get(productId);
        cart.setQuantity(quantity);
        return cart;
    }

    @Override
    public void remove(Long productId) {
        maps.remove(productId);
    }

    @Override
    public void clear() {
        maps.clear();
    }

    @Override
    public Collection<Cart> getAll() {
        return maps.values();
    }

    @Override
    public double getAmount() {
        return maps.values().stream()
                .mapToDouble(item -> item.getQuantity() * item.getPrice())
                .sum();
    }

    @Override
    public int getCount() {
        return maps.values().size();
    }
}
