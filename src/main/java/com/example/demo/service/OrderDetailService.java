package com.example.demo.service;

import com.example.demo.domain.OrderDetail;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;

public interface OrderDetailService {
    List<OrderDetail> findAll();

    List<OrderDetail> findAll(Sort sort);

    List<OrderDetail> findAllById(Iterable<Integer> integers);

    <S extends OrderDetail> List<S> saveAll(Iterable<S> entities);

    void flush();

    <S extends OrderDetail> S saveAndFlush(S entity);

    <S extends OrderDetail> List<S> saveAllAndFlush(Iterable<S> entities);

    @Deprecated
    void deleteInBatch(Iterable<OrderDetail> entities);

    void deleteAllInBatch(Iterable<OrderDetail> entities);

    void deleteAllByIdInBatch(Iterable<Integer> integers);

    void deleteAllInBatch();

    @Deprecated
    OrderDetail getOne(Integer integer);

    OrderDetail getById(Integer integer);

    <S extends OrderDetail> List<S> findAll(Example<S> example);

    <S extends OrderDetail> List<S> findAll(Example<S> example, Sort sort);

    Page<OrderDetail> findAll(Pageable pageable);

    <S extends OrderDetail> S save(S entity);

    Optional<OrderDetail> findById(Integer integer);

    boolean existsById(Integer integer);

    long count();

    void deleteById(Integer integer);

    void delete(OrderDetail entity);

    void deleteAllById(Iterable<? extends Integer> integers);

    void deleteAll(Iterable<? extends OrderDetail> entities);

    void deleteAll();

    <S extends OrderDetail> Optional<S> findOne(Example<S> example);

    <S extends OrderDetail> Page<S> findAll(Example<S> example, Pageable pageable);

    <S extends OrderDetail> long count(Example<S> example);

    <S extends OrderDetail> boolean exists(Example<S> example);

}
