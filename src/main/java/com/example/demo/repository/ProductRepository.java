package com.example.demo.repository;

import com.example.demo.domain.Category;
import com.example.demo.domain.Product;
import jdk.jshell.Snippet;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
    List<Product> findByNameContaining(String name);

    Page<Product> findByNameContaining(String name, Pageable pageable);

    List<Product> findByStatus(Short status);

    Page<Product> findAll(Pageable pageable);

    Page<Product> findByNameContainingAndCategoryAndUnitPriceBetweenAndStatusNot(String name, Category category, Double minVal, Double maxVal, Short status, Pageable pageable);
    Page<Product> findByNameContainingAndUnitPriceBetweenAndStatusNot(String name, Double minVal, Double maxVal, Short status, Pageable pageable);
    Page<Product> findByNameContainingAndStatus(String name, Short status, Pageable pageable);
    List<Product> findByCategoryAndStatus(Category category, Short status);

}
