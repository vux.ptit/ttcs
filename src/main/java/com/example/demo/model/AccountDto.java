package com.example.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountDto {
	@NotEmpty
	private String username;
	@NotEmpty
	private String password;

	private boolean isAmin;
	public AccountDto(String username, String password) {
		this.username = username;
		this.password = password;
		this.isAmin = false;
	}
	private Boolean isEdit = false;
}
