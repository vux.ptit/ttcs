package com.example.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginAdminDto {
	@NotEmpty
	private String username;
	@NotEmpty
	private String password;

	private Boolean rememberMe = false;
}
