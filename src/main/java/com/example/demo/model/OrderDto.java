package com.example.demo.model;

import java.util.Date;

import com.example.demo.domain.Customer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class OrderDto {
	private int orderId;
	private Date orderDate;
	private double amount;
	private Customer customer;
}
