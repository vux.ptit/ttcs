package com.example.demo.model;

import com.example.demo.domain.Order;
import com.example.demo.domain.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class OrderDetailDto {
	private int orderDetailId;
	private Order order;
	private Product product;
	private int quanity;
}
