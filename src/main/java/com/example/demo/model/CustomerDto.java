package com.example.demo.model;

import java.util.Date;
import java.util.Set;

import com.example.demo.domain.Order;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerDto {
	private Long Id;

	private String emailId;

	private String password;

	private String firstName;

	private String lastName;

	private boolean isEnabled;

	private String phone;

	private String address;

	private Set<Order> orders;
}
