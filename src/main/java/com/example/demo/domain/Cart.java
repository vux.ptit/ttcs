package com.example.demo.domain;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class Cart implements Serializable {

    private Long productId;
    private String name;
    private double price;
    private String image;
    private int quantity = 1;
}
